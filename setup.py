#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

import tiramisu_cmdline_parser

setup(
        name='tiramisu_cmdline_parser',
        version=tiramisu_cmdline_parser.__version__,
        packages=find_packages(),
        author="Emmanuel Garette & Tiramisu Team",
        description="command-line parser using Tiramisu.",
	long_description="""
        Python3 parser for command-line options and arguments using Tiramisu engine.
        """,
	include_package_data=True,
	url='https://framagit.org/tiramisu/tiramisu-cmdline-parser',

	classifiers=[
	    "Programming Language :: Python",
	    "Development Status :: 1 - Planning",
	    "License :: LGPLv3",
	    "Natural Language :: French",
	    "Operating System :: OS Independent",
	    "Programming Language :: Python :: 3",
	    "Topic :: Communications",
	    ],
)

parser.add_arguments([StrOption('echo', 'echo the string you use here', properties=('mandatory', 'positional')),
                      BoolOption('verbosity', 'increase output verbosity', default=False)])

